;------------------------------------------
; int strlen(String str)
; String length calculation function
; Input: 
;   eax - str;
; Out: 
;   eax - len of str   
strlen:
    push    ebx
    mov     ebx, eax
 
next_char:
    cmp     byte [eax], 0
    jz      finished
    inc     eax
    jmp     next_char
 
finished:
    sub     eax, ebx
    pop     ebx
    ret
 
 
;------------------------------------------
; void sprint(String str)
; String printing function
; Input: 
;   eax - str;
sprint:
    push    edx
    push    ecx
    push    ebx
    push    eax
    call    strlen
 
    mov     edx, eax   ; edx - strlen
    pop     eax        ; eax - str
 
    mov     ecx, eax   ; ecx - str
    mov     eax, 4     ; write
    mov     ebx, 1     ; STDOUT
    int     80h
 
    pop     ebx
    pop     ecx
    pop     edx
    ret

;------------------------------------------
; void read(String dstStr)
; Read message from STDIN
; Input:
;   eax - dstStr
sread:
    push    edx
    push    ecx
    push    ebx

    mov     ecx, eax  
    push    eax

    mov     eax, 3      ; read
    mov     ebx, 1      ; STDIN
    mov     edx, 100    ; 100 bytes 
    int     80h
  
    pop     eax
    pop     ebx
    pop     ecx
    pop     edx
    ret

;------------------------------------------
; void strIntoStack(String srcStr)
; Write message into stack
; Input:
;   eax - srcStr
str_into_stack:
    push    ecx
    push    ebx

    mov     ebx, eax    ; ebx - srcMessage
    push    eax

_next_char:
    cmp     byte [eax], 0
    jz      _finished
    call    _push
    inc     eax
    jmp     _next_char
 
_finished:
    pop     eax
    pop     ebx
    pop     ecx
    ret

print_1_bytes:
    push    edx
    push    ecx
    push    ebx
    push    eax

    mov     ecx, eax
    mov     eax, 4     ; write
    mov     ebx, 1     ; STDOUT
    mov     edx, 1     ; edx - strlen
    int     80h
    
    pop     eax
    pop     ebx
    pop     ecx
    pop     edx
    ret


;------------------------------------------
; void push(int32 value)
; Push message into stack
; Input:
;   eax - source value
_push:
    push    eax

    mov     eax, _stack
    sub     eax, edx
    cmp     byte eax, 0
    jz      push_error

    pop eax

    sub edx, 1
    push ecx
    mov cl, byte [eax]
    mov byte [edx], cl   
    pop ecx
    ret

push_error:
    mov eax, stack_over
    call sprint
    pop eax
    call quit



;------------------------------------------
; int pop()
; Pop int from stack
; Out:
;   eax - value
_pop:
    mov eax, edx
    add edx, 1
    ret

_print_stack:
    push    ecx
    push    ebx
    push    eax

    mov     eax, _stack
    add     eax, 10
    sub     eax, edx
    mov     ecx, eax
    _print_stack_loop:
        call    _pop
        call    print_1_bytes
        loop    _print_stack_loop

    pop     eax
    pop     ebx
    pop     ecx
    ret

;------------------------------------------
; void exit()
; Exit program and restore resources
quit:
    mov     ebx, 0
    mov     eax, 1
    int     80h
    ret