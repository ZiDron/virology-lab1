%include	'functions.asm' ; include our external file

; Define variables in the data section
SECTION .data
	hello_message:     	db 'Write anything:', 0Ah, 0h
	stack_over			db 'Stack over', 0Ah, 0h

SECTION .bss
	secret 				RESB 20
	user_message: 		RESB 256
	_stack:				RESB 10
	

; Code goes in the text section
SECTION .text
	GLOBAL _start 

_start:
	mov 	edx, _stack 		; edx - start base
	add 	edx, 10

	mov 	eax, hello_message  
	call 	sprint
	mov     eax, user_message
	call 	sread

	mov 	eax, user_message
	call 	str_into_stack
	call 	_print_stack

	mov 	ecx, 5000
	tt:
	call 	_push
	loop 	tt


	mov		eax, secret
	call 	sprint

	call quit 
