section .data
	question db 'What do you want to input?', 0xA ; Declare/store the information "Hello World!" 
	questionln equ $-question
	input db ' ', 0xA
		
section .text
	
	global _start
	
_start:
	mov eax, 4			; 4 = write
	mov ebx, 1			; 1 = stdout
	mov ecx, question	; pointer
	mov edx, questionln	; size of string
	int 0x80			; syscall

	mov eax, 3			; 3 = read
	mov ebx, 1			; 1 = stdin
	mov ecx, input		; pointer
	mov edx, 64			; size of input
	int 0x80			; syscall

	mov eax, 4			; 4 = write
	mov ebx, 1			; 1 = stdout
	mov ecx, input		; the information at memory address question
	mov edx, 5			; size of string
	int 0x80			; syscall

	mov eax, 1			; sys_exit
	mov ebx, 0			; status
	int 0x80